//
//  MidiMessage.h
//  CommandLineTool
//
//  Created by Andrew Warren on 04/10/2015.
//  Copyright (c) 2015 Tom Mitchell. All rights reserved.
//

#ifndef H_MIDIMESSAGE
#define H_MIDIMESSAGE

#include <stdio.h>


class MidiMessage
{
public:
    /**Consturctor*/
    MidiMessage();
    
    /**second constructor*/
    MidiMessage(int initialNoteNumber, int initialVelocity, int initialChannel);
    
    /**destructor*/
    ~MidiMessage();
    
    /**Sets the MIDI note number of the message*/
    void setNoteNumber (int value);
    
    /**Returns the note number of the message*/
    int getNoteNumber()const;
    
    /**Returns the midi note as frequency*/
    float getMidiNoteInHertz() const;
    
    /**Returns the velocity value*/
    int getMidiVelocity() const;
    
    /**Sets the velocity value*/
    void setMidiVelocity (int value);
    
    /**Sets the midi channel number*/
    void setMidiChannel (int value);
    
    /**Returns the midi channel value*/
    int getMidiChannel () const ;
    
    /**Returns the amplitude*/
    float getFloatVelocity () const; //accessor
    
    
private:
    int noteNumber;
    int velo;
    int channel;
};
#endif /* H_MIDIMESSAGE */