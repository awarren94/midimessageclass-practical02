//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <cmath>
#include "MidiMessage.h"

int main (int argc, const char* argv[])
{
    
    // insert code here...
    
    //input value
    int value;
    
    //create object and call the constructor
    MidiMessage note;
    //second consturctor
    //MidiMessage note (48, 60, 5);
    
    std::cout << "default note no: " << note.getNoteNumber() << std::endl;
    std::cout << "default frequency: " << note.getMidiNoteInHertz() << std::endl << std::endl;
    
    std::cout << "enter note number: ";
    std::cin >> value;
    
    note.setNoteNumber(value);
    
    std::cout << "note no: " << note.getNoteNumber() << std::endl;
    std::cout << "frequency: " << note.getMidiNoteInHertz() << std::endl;
    
    std::cout << std::endl << std::endl << "enter velocity (0 - 127): ";
    std::cin >> value;
    //assigns input value to velocity
    note.setMidiVelocity(value);
    
    //passes value through function to create amplitude
    std::cout << "amplitude: " << note.getFloatVelocity() << std::endl;
    
    std::cout << std::endl << "enter midi channel no: ";
    std::cin >> value;
    
    note.setMidiChannel(value);
    
    std::cout << std::endl << "midi channel = " << note.getMidiChannel() << std::endl;
    
    
    
    return 0;
}
//destructor is called when scope has ended (at the brace)

    
    

    return 0;
}
//destructor is called when scope has ended (at the brace)

