//
//  MidiMessage.cpp
//  CommandLineTool
//
//  Created by Andrew Warren on 04/10/2015.
//  Copyright (c) 2015 Tom Mitchell. All rights reserved.
//

#include "MidiMessage.h"
#include <iostream>
#include <cmath>



MidiMessage::MidiMessage()//Constructor
{
    noteNumber = 60;
    velo = 127;
    channel = 1;
    std::cout << std::endl << "CONSTRUCTOR" << std::endl << std::endl;
    
}
MidiMessage::MidiMessage(int initialNoteNumber, int initialVelocity, int initialChannel) //second constructor
{
    noteNumber = initialNoteNumber;
    velo = initialVelocity;
    channel = initialChannel;
}
MidiMessage::~MidiMessage() //destructor
{
    std::cout << std::endl << "DESTRUCTOR" << std::endl << std::endl;
    
}
void MidiMessage::setNoteNumber (int value) //Mutator
{
    if(value > 0)
    {
        noteNumber  = value;
    }
    
}
int MidiMessage::getNoteNumber() const //accessor
{
    return noteNumber;
}
float MidiMessage::getMidiNoteInHertz() const //accessor
{
    return 440 * pow(2, (noteNumber - 69)/ 12.0);
}
int MidiMessage::getMidiVelocity() const //accessor
{
    return velo;
}
void MidiMessage::setMidiVelocity (int value)//mutator
{
    if(value > 0)
    {
        velo = value;
    }
    
}
void MidiMessage::setMidiChannel (int value) //mutator
{
    if(value > 0)
    {
        channel = value;
    }
}
int MidiMessage::getMidiChannel () const //accessor
{
    return channel;
}
float MidiMessage::getFloatVelocity () const //accessor
{
    return velo/127.0;
}